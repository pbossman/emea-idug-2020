--#SET TERMINATOR #
CONNECT TO SERVER1#
CALL ADMIN_INFO_SYSPARM(NULL,?,?)#

-- ####################################################################
-- # Basic SELECT statement
-- # See https://www.ibm.com/support/knowledgecenter/SSEPEK_12.0.0/sqlref/src/tpc/db2z_sql_select.html for complete syntax.
-- ####################################################################
SELECT *
    FROM SYSIBM.SYSTABLES T
    INNER JOIN SYSIBM.SYSINDEXES I
    ON t.creator = i.tbcreator
    and t.name = i.tbname
    WHERE creator = 'SYSIBM%'
    and name like 'SYS%' #
