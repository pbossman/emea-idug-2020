'use strict'
const express = require('express');
const path = require('path');
const app = express();
const port = 8080;
const fs = require('fs');
const https = require('https');
const bodyParser = require('body-parser');
app.use(express.static(path.join(__dirname, 'public')));

const ibmdb = require('ibm_db');
exports.connObj = require('./config/config.json');
const connStr="DSN="+exports.connObj.DSN+";UID="+exports.connObj.UID+";PWD="+ exports.connObj.PWD+";";

// connect, set current schema, current sqlid
var conn=ibmdb.openSync(connStr);
let sqlSet = "SET CURRENT SQLID = '" + exports.connObj.CURSQLID + "'";
conn.query(sqlSet, function (err, result) {
  if (err) throw err;
  console.log(result);
});
sqlSet = "SET CURRENT SCHEMA = '" + exports.connObj.CURSCH + "'";
conn.query(sqlSet, function (err, result) {
  if (err) throw err;
  console.log(result);
});

console.log("Connected to Db2");

// Ensure every incomming request is parsed to json automatically
app.use(bodyParser.urlencoded({ extended: 'true' }));
app.use(bodyParser.json());

// serve web pages from public folder
app.use('/', express.static('public'));

// list all customers
app.get('/customers', function(req, res){
  let sqlStmt =           'SELECT CUSTOMERID AS "id", FIRSTNAME as "firstName", LASTNAME as "lastName" '
  sqlStmt     = sqlStmt + ' , STREET_ADDRESS as "streetAddress", CITY as "city", STATE as "state", ZIP as "zip" '
  sqlStmt     = sqlStmt + ' , AGEGROUPID as "ageGroupId", INCOME_RANGEID as "incomeRangeId", MARITAL_STATUS as "maritalStatus" '
  sqlStmt     = sqlStmt + ' , GENDER as "gender", ETHNICGROUPID as "ethnicGroupId" '
  sqlStmt     = sqlStmt + 'FROM CUSTOMERS ; '
  console.log(sqlStmt)
	conn.query(sqlStmt, function (err, result) {
    if (err) throw err;
    console.log(result);
    res.send(result);
  });
});

// get customer with id
app.get('/customers/:id', function(req, res){
  let sqlStmt =           'SELECT CUSTOMERID AS "id", FIRSTNAME as "firstName", LASTNAME as "lastName" '
  sqlStmt     = sqlStmt + ' , STREET_ADDRESS as "streetAddress", CITY as "city", STATE as "state", ZIP as "zip" '
  sqlStmt     = sqlStmt + ' , AGEGROUPID as "ageGroupId", INCOME_RANGEID as "incomeRangeId", MARITAL_STATUS as "maritalStatus" '
  sqlStmt     = sqlStmt + ' , GENDER as "gender", ETHNICGROUPID as "ethnicGroupId" '
  sqlStmt     = sqlStmt + 'FROM CUSTOMERS WHERE CUSTOMERID = ? ; '
  let recId = parseInt(req.params.id,10);
  console.log(sqlStmt);
  conn.query(sqlStmt, [recId],function (err, result) {
    if (err) throw err;
    console.log(result);
    res.send(result);
  });
});

// PUT /customers/:id - input is particular customers id and new row data, update the row in database.
app.put('/customers/:id', function(req, res){
  let sqlStmt =           'UPDATE CUSTOMERS '
  sqlStmt     = sqlStmt + 'SET FIRSTNAME = ? , LASTNAME = ? , STREET_ADDRESS = ? '
  sqlStmt     = sqlStmt + ', CITY = ? , STATE = ?, ZIP = ? , AGEGROUPID = ? '
  sqlStmt     = sqlStmt + ', INCOME_RANGEID = ? , MARITAL_STATUS = ?, GENDER = ? , ETHNICGROUPID = ? '
	sqlStmt     = sqlStmt + 'WHERE CUSTOMERID = ? ;'
  let x = req.body; 
  let recId = parseInt(req.params.id,10);
	console.log("PUT /customers request body:", x)
	console.log("PUT /customers update statement: ", sqlStmt);
	conn.query(sqlStmt, [x.firstName, x.lastName, x.streetAddress, x.city, x.state, x.zip, x.ageGroupId, x.incomeRangeId, x.maritalStatus, x.gender, x.ethnicGroupId, recId], function (err, result) {
    if (err) throw err;
    console.log("PUT /customers update result.", result);
    res.send(result);
  });
});

// PATCH /customers/:id - input is particular customers id and new row data, update the row in database.
app.patch('/customers/:id', function(req, res){
  let sqlStmt =           'UPDATE CUSTOMERS '
  sqlStmt     = sqlStmt + 'SET FIRSTNAME = ? , LASTNAME = ? , STREET_ADDRESS = ? '
  sqlStmt     = sqlStmt + ', CITY = ? , STATE = ?, ZIP = ? , AGEGROUPID = ? '
  sqlStmt     = sqlStmt + ', INCOME_RANGEID = ? , MARITAL_STATUS = ?, GENDER = ? , ETHNICGROUPID = ? '
	sqlStmt     = sqlStmt + 'WHERE CUSTOMERID = ? ;'
  let x = req.body; 
  let recId = parseInt(req.params.id,10);
	console.log("PUT /customers request body:", x)
	console.log("PUT /customers update statement: ", sqlStmt);
	conn.query(sqlStmt, [x.firstName, x.lastName, x.streetAddress, x.city, x.state, x.zip, x.ageGroupId, x.incomeRangeId, x.maritalStatus, x.gender, x.ethnicGroupId, recId], function (err, result) {
    if (err) throw err;
    console.log("PUT /customers update result.", result);
    res.send(result);
  });
});

// POST /customers - receive new customers row input, insert into database
app.post('/customers', express.json(), function(req, res){
  var sqlStmt = '';
  var x = req.body;
  console.log("POST /customers insert values ", x );
  sqlStmt = "INSERT INTO CUSTOMERS (CUSTOMERID, FIRSTNAME, LASTNAME, STREET_ADDRESS, CITY, STATE, ZIP, AGEGROUPID, INCOME_RANGEID, MARITAL_STATUS, GENDER, ETHNICGROUPID) Values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
  conn.query(sqlStmt, [x.id, x.firstName, x.lastName, x.streetAddress, x.city, x.state, x.zip, x.ageGroupId, x.incomeRangeId, x.maritalStatus, x.gender, x.ethnicGroupId], function (err, result, fields) {
    if (err) throw err;
    res.send(result);
  });
});

// DELETE /customers/:id - for customers input id, delete row in database.
app.delete('/customers/:id', function(req, res){
	let sqlStmt = 'DELETE FROM CUSTOMERS WHERE CUSTOMERID = ? ;'
  let x = req.body; 
  let recId = parseInt(req.params.id,10);
	console.log("DELETE /customers/:id request body:", x)
	console.log("DELETE /customers/:id delete statement: ", sqlStmt);
	conn.query(sqlStmt, [recId], function (err, result) {
    if (err) throw err;
    console.log("DELETE /customers/:id update result.", result);
    res.send(result);
  });
});
// ************************** End of customers API **************************

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
