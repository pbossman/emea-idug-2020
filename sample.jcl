//DCRTZFS  JOB MSGCLASS=H,MSGLEVEL=(1,1),CLASS=E,REGION=0M             
//*                                                                    
//******************************************************************** 
//* IMPORT USER CATALOGS                                               
//******************************************************************** 
//IMPUCAT1 EXEC PGM=IDCAMS                                             
//CATLG    DD  UNIT=3390,VOL=SER=USSPT1,DISP=SHR                       
//SYSPRINT DD SYSOUT=*                                                 
//SYSIN    DD   *                                                      
  IMPORT OBJECTS ((USSPT1.CATALOG  VOLUME(USSPT1)   -                  
         DEVICETYPE(3390))) -                                          
         CONNECT CATALOG(CATALOG.Z23C.MASTER)                          
  DEFINE ALIAS (NAME (USSPT1) RELATE (CATALOG.USSPT1))                 