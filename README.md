This is a demo application that accompanies an International Db2 Users Group EMEA presentation.

The demo application is a simple web application to enable editing of a table in Db2 for z/OS.
The purpose of the application is to introduce:
- Using node.js with Db2 for z/OS
- Show how node.js can be used to create RESTful services
- Show how node.js can make RESTful calls to Db2 for z/OS
- Show how modern frameworks (in this case, ZingGrid) binds to the RESTful service, radically reducing the amount of code required to consume the service

The web front end uses ZingGrid, which binds to a RESTful service to support get list, get record, insert/update/delete of table data.
The web front end is located in the public/customers.html

There are three embodiments of the node.js backend express server.
1. indexDynSql.js - uses node-ibm_db, which creates an ODBC connection to Db2 for z/OS to use dynamic SQL to perform Db2 actions.
2. indexRESTSQL.js - the node.js server makes Db2 for z/OS native rest service calls that are built on SQL statements to perform Db2 actions.
3. indexRESTNSP.js - the node.js server makes Db2 for z/OS native rest service calls that are build on native stored procedures.

